-- Create an ACPI widget
battery_widget = widget({ type = "textbox" })
battery_widget.text = " | Battery | "
battery_widget_timer = timer({ timeout = 10 })
battery_widget_timer:add_signal("timeout",
  function()
    fh = assert(io.popen("acpi | cut -d, -f 2,3 -", "r"))
    battery_widget.text = " |" .. fh:read("*l") .. " | "
    fh:close()
  end
)
battery_widget_timer:start()
