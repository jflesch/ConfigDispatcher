#!/bin/bash

TO_BIND="dev
proc
sys
tmp
etc/resolv.conf
etc/hosts
etc/hostname
dev/pts"

SERVICES="mysql
apache2"

FULL_SCRIPT_PATH="$(readlink -f $0)"
CHROOT_PATH="$1"

if [ "${CHROOT_PATH}" = "" ]
then
	echo "No chroot specified"
	exit $?
fi

if [ -d "${CHROOT_PATH}" ]
then
	CHROOT_TYPE=directory
	CHROOT_MNT_PATH="${CHROOT_PATH}"
	CHROOT_MNT_POINT=$(df "${CHROOT_PATH}" | tail -1 | awk '{ print $6 }')
elif [ -f "${CHROOT_PATH}" ]
then
	CHROOT_TYPE=file
	CHROOT_MNT_PATH="/mnt/chroot"
	CHROOT_MNT_POINT="${CHROOT_MNT_PATH}"
else
	echo "Specified chroot does not exist"
	exit $?
fi

if [ $USER != "root" ]
then
	echo "Switching to root ..."
	sudo "${FULL_SCRIPT_PATH}" "$@"
	exit $?
fi

if [ "${SHELL}" = "/bin/sh" ]
then
	/bin/bash "${FULL_SCRIPT_PATH}" "$@"
	exit $?
fi

echo "Chroot type: ${CHROOT_TYPE}"

mkdir -p ${CHROOT_MNT_PATH}

if [ "$2" != "" ]; then
	ACTION="$2"
else
	ACTION=mount
	if [ -e dev/stdin ]
	then ACTION=umount
	fi
fi

if [ "${ACTION}" = "mount" ] && [ "${CHROOT_TYPE}" = "file" ]
then
	echo "Mounting ${CHROOT_PATH} on ${CHROOT_MNT_PATH} ..."
	if ! mount -o noatime "${CHROOT_PATH}" "${CHROOT_MNT_PATH}"
	then
		echo "Chroot mounting failed !"
		exit 1
	fi
fi

cd "${CHROOT_MNT_PATH}"

if [ "${ACTION}" = "mount" ]
then
	echo "Enabling FS opts"
	mount -o remount,rw,exec,dev,suid "${CHROOT_MNT_POINT}"

	echo "Mounting ..."
	for bind in ${TO_BIND}
	do
		echo "- binding ${bind} ..."
		mount -o bind /${bind} ${bind}
	done

	echo "Starting services ..."
	for service in ${SERVICES}
	do
		chroot . service ${service} start
	done
else
	echo "Stopping services ..."
	for service in ${SERVICES}
	do
		chroot . service ${service} stop
	done

	sleep 3

	echo "Unmounting ..."
	for bind in ${TO_BIND}
	do
		echo "- unbinding ${bind} ..."
		if ! umount ${bind} ; then
			echo "Processes still using it:"
			fuser -Mm ${bind}
		fi
	done

	if [ "${CHROOT_TYPE}" = "file" ]
	then
		umount "${CHROOT_MNT_PATH}"
	fi
fi
