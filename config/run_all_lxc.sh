#!/bin/sh

if [ -z "${CONTAINERS_ONLY}" ] ; then
	CONTAINERS_ONLY=0
fi

if [ "${USER}" != "root" ];
then
	sudo CONTAINERS_ONLY=${CONTAINERS_ONLY} $0 "$@"
	exit $?
fi

if [ "${CONTAINERS_ONLY}" = "0" ] ; then
	echo "=== local ==="
	echo "$@"
	if "$@" ;
	then
		echo "Done"
	else
		echo "WARNING: command returned an error: $?"
	fi
fi

for container in $(lxc-ls --active)
do
	echo "=== ${container} ==="
	if lxc-attach -n "${container}" -- "$@"
	then
		echo "Done"
	else
		echo "WARNING: command returned an error: $?"
	fi
done
