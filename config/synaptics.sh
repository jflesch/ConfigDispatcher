#!/usr/bin/env bash

# Run when X starts

if [ "$1" = "wait" ]
then sleep 10
fi
/usr/bin/env gsynaptics-init
/usr/bin/env synclient TapButton1=0
/usr/bin/env synclient TapButton2=3
/usr/bin/env synclient TapButton3=2
/usr/bin/env synclient VertTwoFingerScroll=1
/usr/bin/env synclient HorizTwoFingerScroll=1
