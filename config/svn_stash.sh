#!/bin/sh

SVN_STASH_DIR=${HOME}/.svn_stash

mkdir -p ${SVN_STASH_DIR}

if [ "$1" = "stash" ] || [ -z "$1" ]; then

	stash_name=$(svn info | grep "Relative URL" | cut -d' ' -f3-)
	stash_name=$(echo ${stash_name} | sed "s/[^a-zA-Z0-9/]//g")
	stash_name=$(echo ${stash_name} | sed "s#^/##g")
	stash_name=$(echo ${stash_name} | sed "s#/#_#g")
	stash_name="$(date "+%Y%m%d_%M%S")_${stash_name}"
	echo "Stashing: ${stash_name}"

	svn diff > ${SVN_STASH_DIR}/${stash_name}.diff

	if [ $(stat -c "%s" "${SVN_STASH_DIR}/${stash_name}.diff") -eq 0 ] ; then
		echo "Nothing to stash"
		rm -f "${SVN_STASH_DIR}/${stash_name}.diff"
		exit 1
	fi

	svn revert -qR .

elif [ "$1" = "list" ]; then

	(cd ${SVN_STASH_DIR} ; ls -1 | sort)

elif [ "$1" = "apply" ] ; then

	patch -p0 < "${SVN_STASH_DIR}/$2"

elif [ "$1" = "show" ] ; then

	cat "${SVN_STASH_DIR}/$2" | colordiff | most

elif [ "$1" = "rm" ] ; then

	rm "${SVN_STASH_DIR}/$2"

else

	echo "Unknown command: $1"

fi
