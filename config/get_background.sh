#!/usr/bin/env bash

if ! [ -e "${HOME}/git/configdispatcher/scripts/common" ]
then
	echo "Can't load configdispatcher common thingies"
	exit 1
fi
export OS=$(uname)
source ${HOME}/git/configdispatcher/scripts/common > /dev/null
source ${HOME}/git/configdispatcher/scripts/${OS}.env > /dev/null

if [ -e ${HOME}/Sync/wallpapers ]
then BACKGROUNDS=${HOME}/Sync/wallpapers
else
    echo "Can't find wallpapers"
    exit 1
fi

res=$(xrandr \
	| grep connected \
	| grep -v disconnected \
	| grep -v "connected primary (normal" \
	| sed "s/.*connected //g" \
	| sed "s/primary //" \
	| sed "s/ .*//g" \
	| sort -t+ -k2 -n \
	| head -n1 \
	| sed "s/+.*//g")

if [ "$res" = "1920x1080" ] ; then
	ratio=16_9
elif [ "$res" = "1680x1050" ] ; then
	ratio=16_10
elif [ "$res" = "1080x1920" ] ; then
	ratio=9_16
else
	ratio="${res}"
fi

bg_files="${BACKGROUNDS}/${ratio}/*""$1"
bg_nmb_files=$(echo $bg_files | wc -w)
bg_rnd_file=$(echo $bg_files | cut -d' ' -f $(( (${RANDOM} % ${bg_nmb_files}) + 1 )) )
echo "${bg_rnd_file}"
