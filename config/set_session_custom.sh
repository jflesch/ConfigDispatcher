#!/usr/bin/env bash

if [ -z "${DISPLAY}" ]
then export DISPLAY=:0.0
fi

if [ -e /usr/local/bin/osd_cat ]
then XOSD=/usr/local/bin/osd_cat
else XOSD=/usr/bin/osd_cat
fi

if ! [ -x ${XOSD} ]; then
	echo "Xosd is not installed. Nothing to do"
	exit 0
fi

osd_full_green_list="/var/log/auth.log"
osd_full_white_list="/var/log/daemon.log /var/log/lpr.log /var/log/mail.log /var/log/user.log"
osd_full_cyan_list="/var/log/dmesg /var/log/messages /var/log/syslog /var/log/Xorg.0.log"
osd_full_orange_list="/var/log/dpkg.log /var/log/mail.warn"
osd_full_red_list="/var/log/faillog /var/log/mail.err"

function check_file_list()
{
	export new_list=""
	for file in "$@"
	do
		if [ -f ${file} ] && [ -r ${file} ]
		then
			new_list="${new_list}${file} "
		fi
	done
	return
}

check_file_list ${osd_full_green_list}
osd_real_green_list=${new_list}
check_file_list ${osd_full_white_list}
osd_real_white_list=${new_list}
check_file_list ${osd_full_cyan_list}
osd_real_cyan_list=${new_list}
check_file_list ${osd_full_orange_list}
osd_real_orange_list=${new_list}
check_file_list ${osd_full_red_list}
osd_real_red_list=${new_list}

echo "---"
echo "OSD green list:	${osd_real_green_list}"
echo "OSD white list:	${osd_real_white_list}"
echo "OSD cyan list:	${osd_real_cyan_list}"
echo "OSD orange list:	${osd_real_red_list}"
echo "OSD red list: 	${osd_real_orange_list}"
echo "---"

function start_osd()
{
	local color=$1
	shift;
	if [ -z "$1" ];
	then return 0
	fi
	echo -n "Starting OSD for ${color} files: " "$@" " ... "
	tail -n 0 -f "$@" | ${XOSD} -i 150 -O 3 -u black -l 2 -c ${color} &
	echo -e ${ANSI_GREEN_TXT}"OK"${ANSI_RST_TXT}
}

echo "Starting XOSD instances ... "
killall tail 2> /dev/null > /dev/null
killall osd_cat 2>/dev/null >/dev/null
start_osd green ${osd_real_green_list}
start_osd white ${osd_real_white_list}
start_osd cyan ${osd_real_cyan_list}
start_osd orange ${osd_real_orange_list}
start_osd red ${osd_real_red_list}

