#!/usr/bin/env bash

if ! [ -e "${HOME}/git/configdispatcher/scripts/common" ]
then
	echo "Can't load configdispatcher common thingies"
	exit 1
fi
export OS=$(uname)
source ${HOME}/git/configdispatcher/scripts/common
source ${HOME}/git/configdispatcher/scripts/${OS}.env

if [ -z "${DISPLAY}" ]
then export DISPLAY=:0.0
fi

if [ -e /usr/local/bin/feh ]
then FEH=/usr/local/bin/feh
else FEH=/usr/bin/feh
fi
if [ -e /usr/local/bin/convert ]
then CONVERT=/usr/local/bin/convert
else CONVERT=/usr/bin/convert
fi

if [ -e ${HOME}/Sync/wallpapers ]
then BACKGROUNDS=${HOME}/Sync/wallpapers
else
	echo "Can't find wallpapers"
	exit 1
fi


screen_res=$(xrandr \
	| grep connected \
	| grep -v disconnected \
	| grep -v "connected primary (normal" \
	| sed "s/.*connected //g" \
	| sed "s/primary //" \
	| sed "s/ .*//g" \
	| sort -t+ -k2 -n)

if ! ${CONVERT} -size 0x0 xc:#FF00FF ${HOME}/tmp/wallpaper.jpg
then
	echo "Woops, creation of init wallpaper failed"
	exit 1
fi

for res_pos in ${screen_res}
do
	res=$(echo ${res_pos} | sed "s/+.*//g")
	len=$(echo ${res_pos} | sed "s/x.*//g")
	pos=$(echo ${res_pos} | sed "s/.*+//g")

	if [ "$res" = "1920x1080" ] || [ "$res" = "1366x768" ] ; then
		ratio=16_9
	elif [ "$res" = "1680x1050" ] ; then
		ratio=16_10
	elif [ "$res" = "1080x1920" ] ; then
		ratio=9_16
	else
		ratio="${res}"
	fi

	if ! [ -d "${BACKGROUNDS}/${ratio}" ] ; then
		echo "ERROR: Unknown resolution: ${res} (${ratio})"
		exit 1
	fi

	bg_files="${BACKGROUNDS}/${ratio}/*"
	bg_nmb_files=$(echo $bg_files | wc -w)
	bg_rnd_file=$(echo $bg_files | cut -d' ' -f $(( (${RANDOM} % ${bg_nmb_files}) + 1 )) )
	echo "---"
	echo "Background dir:		${bg_set}"
	echo "Nmb background files:	${bg_nmb_files}"
	echo "Selected background:	${bg_rnd_file}"
	echo "---"
	rm -f ${HOME}/tmp/resized.jpg
	if ! ${CONVERT} ${bg_rnd_file} -resize ${res} ${HOME}/tmp/resized.jpg ; then
		echo "Wallpaper resize failed"
		exit 1
	fi
	rm -f ${HOME}/tmp/padded.jpg
	if [ ${pos} -gt 0 ] ; then
		if ! ${CONVERT} -size ${len}x${pos} "xc:#FF00FF" ${HOME}/tmp/padding.jpg \
			|| ! ${CONVERT} ${HOME}/tmp/padding.jpg ${HOME}/tmp/resized.jpg -append ${HOME}/tmp/padded.jpg ; then
			echo "Wallpaper creation failed"
			exit 1
		fi
	else
		ln -s ${HOME}/tmp/resized.jpg ${HOME}/tmp/padded.jpg
	fi
	if ! ${CONVERT} ${HOME}/tmp/wallpaper.jpg ${HOME}/tmp/padded.jpg +append ${HOME}/tmp/wallpaper2.jpg ; then
		echo "Wallpaper creation failed (2)"
		exit 1
	fi
	mv -f ${HOME}/tmp/wallpaper2.jpg ${HOME}/tmp/wallpaper.jpg
done

# assuming no space in path

echo -n "Updating background ... "
if ${FEH} --bg-tile ${HOME}/tmp/wallpaper.jpg
then echo -e ${ANSI_GREEN_TXT}"OK"${ANSI_RST_TXT}
else echo -e ${ANSI_RED_TXT}"KO"${ANSI_RST_TXT}
fi

