#!/usr/bin/env bash

if [ -z "$FORK" ];
then
	export FORK=1
	$0 "$@" &
	exit 0
fi

if ! [ -e "${HOME}/git/configdispatcher/scripts/common" ]; then
	echo "Can't load configdispatcher common thingies" >&2
	exit 1
fi
export OS=$(uname)
source ${HOME}/git/configdispatcher/scripts/common > /dev/null
source ${HOME}/git/configdispatcher/scripts/${OS}.env > /dev/null

config_get_value "espeak_voice" "Espeak voice ?" > /dev/null
user_voice="${value}"

case $1 in
	"fr")
		lang="fr"
		shift
		;;
	"en")
		lang="en"
		shift
		;;
esac

case $user_voice in
	"male")
		voice="${lang}"
		;;
	"female")
		voice="${lang}+f5"
		;;
	*)
		echo "Unknow voice: "${user_voice}
		;;
esac

# first we have to reduce music volume

if [ -x "$(which rhythmbox-client 2> /dev/null)" ]
then
	for i in $(${SEQ} 1 3)
	do
		rhythmbox-client --no-start --no-present --volume-down
	done
fi

if [ -x "$(which cmus-remote 2> /dev/null)" ]
then
	cmus-remote -v -20 2>/dev/null
	# for this to work, cmus must be set to use software volume (:toggle softvol)
	# however the drawback with that is that volume changes are only applied at the end of the sound buffer
	sleep 1
fi

espeak -s 155 -v${voice} "$@"

if [ -x "$(which rhythmbox-client 2> /dev/null)" ]
then
	for i in $(${SEQ} 1 3)
	do
		rhythmbox-client --no-start --no-present --volume-up
	done
fi

if [ -x "$(which cmus-remote 2> /dev/null)" ]
then cmus-remote -v +20 2>/dev/null
fi


