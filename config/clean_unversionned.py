#!/usr/bin/env python3

import os
import re


def remove(fullpath):
    print("Removing {} (F)".format(fullpath))

    path = ""
    for p in os.path.split(fullpath):
        path = os.path.join(path, p)
        if os.path.islink(path):
            print("--> Removing {} (L)".format(path))
            os.remove(path)
            return

    os.remove(fullpath)


def rmdir(path):
    for x in os.listdir(path):
        fullpath = os.path.join(path, x)
        if os.path.islink(fullpath):
            print("--> Removing {} (L)".format(path))
            os.remove(fullpath)
    os.rmdir(path)


def removeall(path):
    if not os.path.isdir(path):
        remove(path)
        return
    files = os.listdir(path)
    for x in files:
        fullpath = os.path.join(path, x)
        if not os.path.exists(fullpath):
            # probably related to a removed symlink
            continue
        if os.path.isfile(fullpath):
            remove(fullpath)
        elif os.path.isdir(fullpath):
            removeall(fullpath)

    if not os.path.exists(path):
        # probably related to a removed symlink
        return
    print("Removing {} (D)".format(path))
    rmdir(path)


if __name__ == "__main__":
    is_git_repo = True
    for l in os.popen('git status'):
        if 'fatal' in l:
            is_git_repo = False
    if is_git_repo:
        for l in os.popen('git clean -fdx').readlines():
            l = l.strip()
            print(l)
    else:
        unversionedRex = re.compile('^ ?[\?ID] *[1-9 ]*[a-zA-Z]* +(.*)')
        for l in os.popen('svn status --no-ignore -v').readlines():
            match = unversionedRex.match(l)
            if match:
                removeall(match.group(1))
