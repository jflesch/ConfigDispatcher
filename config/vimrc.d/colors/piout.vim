" Vim color file
"
" Maintainer:   Alexandre Belloni <alexandre.belloni@piout.net>
" Last Change:  18 May 2006
" URL:          http://piout.net


set background=light

hi clear
if exists("syntax_on")
  syntax reset
endif

let g:colors_name="piout"


"= Syntax Groups =============================================

hi comment      guibg=#fafafa   guifg=#339900   gui=italic

hi constant     guibg=#fafafa   guifg=#dd3333   gui=none
hi specialchar  guibg=#fafafa   guifg=#ff3333   gui=underline

hi identifier   guibg=#fafafa   guifg=#6666ff   gui=none

hi statement    guibg=#fafafa   guifg=#3c9cfc   gui=bold
hi label        guibg=#fafafa   guifg=#999999   gui=underline

hi preproc      guibg=#fafafa   guifg=#339900   gui=none
hi include      guibg=#fafafa   guifg=#3c9cfc   gui=none
hi precondit    guibg=#fafafa   guifg=#339900   gui=bold

hi type         guibg=#fafafa   guifg=#3c9cfc   gui=none

hi special      guibg=#fafafa   guifg=#cc00cc   gui=none

hi error        guibg=#ff3333   guifg=#ffffff   gui=none

hi todo         guibg=#339933   guifg=#ffffff   gui=none


"= General Groups ============================================

hi cursor       guibg=#000000   guifg=#ffffff   gui=none

hi directory    guibg=#fafafa   guifg=#3c9cfc   gui=none

hi diffadd      guibg=#66ff66   guifg=#000000   gui=none
hi diffdelete   guibg=#ff6666   guifg=#ff6666   gui=none
hi diffchange   guibg=#ffff00   guifg=#cccc99   gui=none
hi difftext     guibg=#ffff00   guifg=#000000   gui=none

hi errormsg     guibg=#ff3333   guifg=#ffffff   gui=none

hi vertsplit    guibg=#3c9cfc   guifg=#3c9cfc   gui=none

hi folded       guibg=#eeeeee   guifg=#3c9cfc   gui=none
hi foldcolumn   guibg=#eeeeee   guifg=#999999   gui=none

hi linenr       guibg=#eeeeee   guifg=#999999   gui=none

hi modemsg      guibg=#fafafa   guifg=#999999   gui=none

hi moremsg      guibg=#339900   guifg=#ffffff   gui=none
hi question     guibg=#339900   guifg=#ffffff   gui=none

hi nontext      guibg=#fafafa   guifg=#999999   gui=none

hi normal       guibg=#fafafa   guifg=#000000   gui=none

hi search       guibg=#ffff00   guifg=#000000   gui=none
hi incsearch    guibg=#ffbb00   guifg=#000000   gui=none

hi specialkey   guibg=#fafafa   guifg=#cc00cc   gui=none

hi statusline   guibg=#3c9cfc   guifg=#ffffff   gui=bold
hi statuslinenc guibg=#3c9cfc   guifg=#bfbfbf   gui=bold

hi title        guibg=#fafafa   guifg=#6666ff   gui=none

hi visual       guibg=#bbbbbb   guifg=#333333   gui=none

hi warningmsg   guibg=#fafafa   guifg=#ff0000   gui=none

hi wildmenu     guibg=#339900   guifg=#ffffff   gui=none

if version >= 700
"  hi CursorLine guibg=#f6f6f6
"  hi CursorColumn guibg=#eaeaea
  hi MatchParen guibg=#3c9cfc	guifg=#ffffff	gui=bold

  "Tabpages
"  hi TabLine guifg=black guibg=#b0b8c0 gui=italic
"  hi TabLineFill guifg=#9098a0
"  hi TabLineSel guifg=black guibg=#f0f0f0 gui=italic,bold

  "P-Menu (auto-completion)
  hi Pmenu		guibg=#3c9cfc	guifg=#ffffff	gui=none
  "PmenuSel
  "PmenuSbar
  "PmenuThumb
endif

" [eof]

