;; -*- mode: emacs-lisp; auto-compile-lisp: nil; -*-

(custom-set-variables
  ;; custom-set-variables was added by Custom.
  ;; If you edit it by hand, you could mess it up, so be careful.
  ;; Your init file should contain only one such instance.
  ;; If there is more than one, they won't work right.
 '(blink-cursor-mode nil nil (frame))
 '(case-fold-search nil)
 '(column-number-mode t)
 '(custom-file "~/.emacs-cust")
 '(delete-selection-mode nil nil (delsel))
 '(display-time-mode t)
 '(ecb-layout-name "left3")
 '(ecb-options-version "2.32")
 '(emacsw32-max-frames nil)
 '(fill-column 79)
 '(global-hl-line-mode t)
 '(highlight-nonselected-windows t)
 '(icicle-reminder-prompt-flag 15)
 '(ido-everywhere t t)
 '(next-line-add-newlines nil)
 '(nxhtml-skip-welcome t)
 '(put (quote upcase-region) t)
 '(require-final-newline nil)
 '(scroll-bar-mode (quote right))
 '(sh-indentation 2)
 '(sh-shell-file "/bin/sh")
 '(show-paren-mode t)
 '(standard-indent 4)
 '(track-eol t)
 '(transient-mark-mode t)
 '(truncate-partial-width-windows t)
 '(view-read-only nil)
)
(custom-set-faces
  ;; custom-set-faces was added by Custom.
  ;; If you edit it by hand, you could mess it up, so be careful.
  ;; Your init file should contain only one such instance.
  ;; If there is more than one, they won't work right.
 '(diff-added ((t (:foreground "DarkGreen"))))
 '(diff-changed ((t (:foreground "MediumBlue"))))
 '(diff-context ((t (:foreground "Black"))))
 '(diff-file-header ((t (:foreground "Red" :background "White"))))
 '(diff-header ((t (:foreground "Red"))))
 '(diff-hunk-header ((t (:foreground "White" :background "Salmon"))))
 '(diff-index ((t (:foreground "Green"))))
 '(diff-nonexistent ((t (:foreground "DarkBlue"))))
 '(diff-removed ((t (:foreground "DarkMagenta"))))
 '(tabbar-selected-face ((t (:inherit tabbar-default-face :foreground "blue" :box (:line-width 2 :color "white" :style pressed-button)))))
 '(tabbar-unselected-face ((t (:inherit tabbar-default-face :box (:line-width 2 :color "white" :style released-button)))))
; '(font-lock-warning-face ((t (:foreground "Red1"))))
)

(setq auto-mode-alist (cons '("\.lua$" . lua-mode) auto-mode-alist))
(autoload 'lua-mode "lua-mode" "Lua editing mode." t)

(color-theme-ld-dark)

(set-frame-font "Liberation Mono-10")
