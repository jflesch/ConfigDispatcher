#!/bin/bash

for sink in $(pactl list short sinks | cut -f1 -d$'\t'); do
	pactl set-sink-volume ${sink} -- "$@"
done

