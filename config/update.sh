#!/bin/sh

if [ "${USER}" != "root" ];
then
	sudo $0 "$@"
	exit $?
fi

echo "=== local ==="
echo "# apt update"
apt update
echo "# apt dist-upgrade"
if ! apt -y dist-upgrade
then
	exit $?
fi

if ! which lxc-ls > /dev/null
then
	exit 0
fi

for container in $(lxc-ls --active)
do
	echo "=== ${container} ==="

	if ! lxc-attach -n "${container}" -- cat /etc/apt/sources.list | grep security > /dev/null
	then
		echo "WARNING: security.debian.org missing. Will add."
		lxc-attach -n "${container}" -- sh -c 'echo "deb http://security.debian.org/ stable/updates main contrib" >> /etc/apt/sources.list'
		echo "Done"
	fi

	echo "# apt update"
	lxc-attach -n "${container}" -- apt update
	echo "# apt dist-upgrade"
	if ! lxc-attach -n "${container}" -- sh -c 'unset SHELL ; apt -y dist-upgrade'
	then
		exit $?
	fi
done
