#!/usr/bin/env bash

# Main system elements check

if [ "$UID" -eq 0 ]
then
    echo "DO NOT RUN AS ROOT"
    exit 128
fi

export PATH=/sbin:/usr/sbin:${PATH}

cd $(dirname $0)

source scripts/common

echo "------"
echo "Check"
echo "------"

echo -n "Checking OS type ... "
export OS=$(uname)
if ! [ -f scripts/${OS}.env ]
then
    echo
    echo -e "Unknown OS: "${ANSI_RED_TXT}${OS}${ANSI_RST_TXT}
    exit 1
fi
echo -e ${ANSI_GREEN_TXT}${OS}${ANSI_RST_TXT}

source scripts/${OS}.env

check_cmd sudo sudo 1 sudo sudo security/sudo

if [ "${OS}" = "FreeBSD" ]
then
    check_cmd GCC gcc 1
    check_cmd "GNU Make" ${MAKE} 1
else
    check_cmd GCC gcc 0 build-essential gcc
    check_cmd "GNU Make" ${MAKE} 0 make make
fi

check_cmd "GNU Sed" ${SED} 1 "coreutils" "coreutils" "textproc/gsed"
check_cmd "GNU Seq" ${SEQ} 1 "coreutils" "coreutils" "sysutils/coreutils"

check_cmd GIT git 1 git-core git devel/git
export GIT=$?
check_cmd Subversion svn 1 subversion subversion devel/subversion
export SUBVERSION=$?
check_cmd Wget wget 1 wget wget ftp/wget
check_cmd Zsh zsh 1 zsh zsh shells/zsh
check_cmd Man man 1 manpages-dev man-pages
check_cmd Screen screen 1 screen screen sysutils/screen
# check_cmd Htop htop 1 htop htop sysutils/htop
check_cmd Most most 1 most most sysutils/most
check_cmd Tcpdump tcpdump 1 tcpdump tcpdump net/tcpdump
check_cmd Nmap nmap 0 nmap nmap security/nmap
check_cmd Pwgen pwgen 1 pwgen pwgen sysutils/pwgen
check_cmd Rsync rsync 1 rsync rsync net/rsync
check_cmd Httpie http 1 httpie
check_cmd Sshfs sshfs 1 sshfs
check_cmd Encfs encfs 1 encfs

check_cmd Vim vim 1 vim vim editors/vim
export VIM=$?

if [ ${VIM} -gt 0 ] ; then
    check_cmd Flawfinder flawfinder 1 flawfinder flawfinder
    check_cmd Splint splint 1 splint splint
    check_cmd Flake8 flake8 1 python3-flake8 python3-flake8
fi

# gvim var will be updated later
export GVIM=0
check_cmd Lirc lircd 0 lirc lirc comms/lirc
export LIRC=$?
check_cmd SSH /usr/sbin/sshd 1 ssh ssh
check_cmd OpenVPN openvpn 0 openvpn openvpn net/openvpn
export OPENVPN=$?
check_cmd Tudu tudu 0 tudu tudu XXX
export TUDU=$?

check_cmd Pip pip 1 python-pip python-pip XXX
check_cmd Pip3 pip3 1 python3-pip python3-pip XXX
check_cmd Ipython ipython 1 ipython ipython XXX
check_cmd Ipython3 ipython3 1 ipython3 ipython3 XXX

check_cmd SilverSearcher ag 1 silversearcher-ag

check_cmd lnav lnav 1 lnav
export LNAV=$?

check_cmd Xorg X 0 xorg xorg x11-servers/xorg-server
export XORG=$?

if [ $XORG -eq 1 ]
then
    check_cmd Xterm	xterm 1 xterm xterm x11/xterm
    export XTERM=$?
    check_cmd Rxvt-Unicode urxvt 0 rxvt-unicode-lite rxvt-unicode x11/rxvt-unicode
    export URXVT=$?
    check_cmd Awesome awesome 0 awesome awesome x11-wm/awesome
    export AWESOME=$?
    if [ $AWESOME -gt 0 ]
    then check_cmd Feh feh 1 feh feh graphics/feh
    check_cmd Pasystray pasystray 0 pasystray
    export PASYSTRAY=$?
    fi

    check_cmd Mplayer mplayer 0 mplayer mplayer multimedia/mplayer

    export GDM=0
    if ! check_cmd Gdm gdm 0 gdm gdm
    then export GDM=1
    elif ! check_cmd Gdm gdm3 0 gdm gdm
    then export GDM=1
    fi

    export RHYTHMBOX=0
    export CMUS=0
    export MUSIC_PLAYER=0
    check_cmd Rhythmbox rhythmbox 0 rhythmbox rhythmbox audio/rhythmbox
    if [ $? -gt 0 ]
    then
        export RHYTHMBOX=1
        export MUSIC_PLAYER=1
    fi
    check_cmd Cmus cmus 0 cmus cmus audio/cmus
    if [ $? -gt 0 ]
    then
        export CMUS=1
        export MUSIC_PLAYER=1
    fi

    check_cmd Gnome-settings-daemon gnome-settings-daemon 0 gnome-settings-daemon gnome-settings-daemon sysutils/gnome-settings-daemon
    export GNOME_SETTINGS_DAEMON=$?
    check_cmd Gnome-power-manager gnome-power-manager 0 gnome-power-manager gnome-power-manager sysutils/gnome-power-manager
    export GNOME_POWER_MANAGER=$?
    check_cmd Bluetooth-applet bluetooth-applet 0
    export BLUETOOTH_APPLET=$?
    check_cmd Gnome-volume-control-applet gnome-volume-control-applet 0
    export GNOME_VOLUME_CONTROL_APPLET=$?
    check_cmd Update-notifier update-notifier 0
    export UPDATE_NOTIFIER=$?
    check_cmd Network-manager nm-applet 0 network-manager-gnome network-manager-applet
    export NM_APPLET=$?
    check_cmd Empathy empathy 0 empathy empathy net-im/empathy
    export EMPATHY=$?
    check_cmd Pidgin pidgin 0 pidgin pidgin net-im/pidgin
    export PIDGIN=$?
    check_cmd Psi psi 0 psi psi
    export PSI=$?
    check_cmd XScreenSaver xscreensaver 0 xscreensaver xscreensaver x11/xscreensaver
    export XSCREENSAVER=$?

    if ! check_cmd Firefox firefox 0
    then export FIREFOX=firefox
    elif ! check_cmd Firefox3 firefox3 0
    then export FIREFOX=firefox3
    else
        check_cmd Iceweasel iceweasel 1 firefox firefox www/firefox35
        export FIREFOX=iceweasel
    fi

    check_cmd ck-launch-session ck-launch-session 0
    export CK_LAUNCH_SESSION=$?

    export CHROME=""
    if [ ${OS} != "FreeBSD" ]
    then
        if ! check_cmd Chrome chrome 0
        then export CHROME="chrome"
        elif ! check_cmd Chromium chromium 0
        then export CHROME="chromium"
        elif ! check_cmd Chromium chromium-browser 0
        then export CHROME="chromium-browser"
        fi
    fi

    # Geeqie is the successor of gqview
    # Note: the following code may not work on old linux or on FreeBSD
    export GQVIEW=""
    if ! check_cmd Gqview gqview 0
    then export GQVIEW="gqview"
    elif ! check_cmd Geeqie geeqie 1 geeqie geeqie graphics/geeqie
    then export GQVIEW="geeqie"
    fi
    check_cmd GVim gvim 1 vim-gnome gvim editors/vim
    export GVIM=$?
    check_cmd Synergy synergyc 0 synergy synergy sysutils/synergy
    export SYNERGY=$?
    # diffuse is used by the git config
    check_cmd Diffuse diffuse 1 diffuse diffuse devel/diffuse
    check_cmd Xcompmgr xcompmgr 0 xcompmgr xcompmgr x11/compmgr
    export XCOMPMGR=$?

    check_cmd ImageMagick convert 1 imagemagick imagemagick graphics/ImageMagick
    check_cmd Espeak espeak 1 espeak espeak audio/espeak

    check_cmd SmartNotifier smart-notifier 0 smart-notifier smart-notifier
    export SMART_NOTIFIER=$?

    check_cmd RedShift redshift-gtk 0 gtk-redshift gtk-redshift accessibility/redshift
    export REDSHIFT=$?

    check_cmd keepassxc keepassxc 1 keepassxc keepassxc
    check_cmd i3lock i3lock 1 i3lock
fi

check_cmd Emacs emacs 0 emacs23 emacs editors/emacs
if [ $? -eq 0 ]
then export EMACS=0
else
    export EMACS=$(emacs --version \
        | grep "GNU Emacs" \
        | grep -v "ABSOLUTELY NO WARRANTY" \
        | ${SED} s/"GNU Emacs "//g \
        | cut -d'.' -f1 \
        2> /dev/null)
    if [ -z "$EMACS" ]
    then
        echo "Unknown version of emacs:"
        emacs --version
        export EMACS=0
    else
        echo "Emacs v"${EMACS}" found"
    fi
fi

if [ $EMACS -ge 23 ] || [ $GVIM -gt 0 ]
then
    check_cmd Gtags gtags 1 global
fi

# Let's play
export TMP_OUTPUT=$(mktemp /tmp/configdispatcher.XXXXX)

echo "------"
echo "Updating config"
echo "------"
echo "(Detailled output: "${TMP_OUTPUT}")"
echo
if scripts/main.sh
then
    echo "------"
    echo -e "I'm making a note here: "${ANSI_GREEN_TXT}"HUGE SUCCESS"${ANSI_RST_TXT}
    echo "------"
    rm -f ${TMP_OUTPUT}
else
    echo "------"
    echo -e ${ANSI_RED_TXT}"Woops."${ANSI_RST_TXT}" Looks like I broke something ..."
    echo "------"
    tail -n 20 ${TMP_OUTPUT}
fi
