#!/usr/bin/env bash

source scripts/common

if [ ${XORG} -eq 0 ]
then exit 0
fi

image_sets=""
if [ -d ${HOME}/git/home/Documents/photos ]
then image_sets="${image_sets} ${HOME}/git/home/Documents/photos"
fi
if [ -d ${HOME}/rsync/backgrounds ]
then image_sets="${image_sets} ${HOME}/rsync/backgrounds ${HOME}/rsync/backgrounds/*"
fi

config_get_value "xscreensaver_images" "Xscreensaver image set" ${image_sets}
set=${value}

if [ -x $(which glxinfo 2> /dev/null) ] && glxinfo > /dev/null 2>&1
then
	echo "GLX seems to be working. Activating 3d screensavers"
	src_file=config/xscreensaver.3d
else
	echo -e "GLX "${ANSI_YELLOW_TXT}"disabled"${ANSI_RST_TXT} \
		" or glxinfo "${ANSI_YELLOW_TXT}"missing"${ANSI_RST_TXT}"." \
		" Using only 2d screensavers"
	src_file=config/xscreensaver.2d
fi

rm -f ${HOME}/.xscreensaver
cp ${src_file} ${HOME}/.xscreensaver
modify_file_var ${HOME}/.xscreensaver "%IMAGES%" "${set}"
killall -SIGHUP xscreensaver 2> /dev/null >&2
exit 0
