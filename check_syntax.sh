#!/usr/bin/env bash

SCRIPTS="$(ls -1 ./*.sh)
$(find scripts -type f -executable)"

for file in ${SCRIPTS}
do
    echo -n "Checking "${file}" ... "
    if bash -n ${file}
    then echo "OK"
    else
	echo "KO"
	exit 1
    fi
done
