#!/usr/bin/env bash

source ./scripts/common

has_repo=0

# My zshrc is basically a fork of http://asyd.net/home/zsh
REPOS="liquidprompt=https://github.com/nojhan/liquidprompt.git
zsh=git@framagit.org:jflesch/Zshrc.git"

for repo in ${REPOS} ; do
	repodir=$(echo ${repo} | cut -f1 -d=)
	repourl=$(echo ${repo} | cut -f2 -d=)

	if [ -e ${HOME}/git/${repodir} ]
	then
		echo -n "Found ${HOME}/git/${repodir}. Updating ... "
		(
		cd ${HOME}/git/${repodir}
		do_or_fail git pull
		)
	else
		echo -n "${HOME}/git/${repodir} not found. Cloning ... "
		(
		cd ${HOME}/git
		do_or_fail git clone ${repourl} ${repodir}
		)
	fi

done

if ! echo "${SHELL}" | grep zsh > /dev/null; then
	echo "Calling 'chsh -s $(which zsh)'"
	chsh -s $(which zsh)
fi

rm -rf ${HOME}/.zsh
rm -f ${HOME}/.zshrc
ln -s ${HOME}/git/zsh ${HOME}/.zsh
ln -s ${HOME}/git/zsh/zshrc ${HOME}/.zshrc

mkdir -p ~/.config
do_or_fail cp -f config/liquidprompt.ps1 ~/.config/liquidprompt.ps1
do_or_fail cp -f config/liquidpromptrc ~/.liquidpromptrc

echo -n "Copying update.sh ... "
do_or_fail cp config/update.sh ${HOME}/bin

echo -n "Copying run_all_lxc.sh ... "
do_or_fail cp config/run_all_lxc.sh ${HOME}/bin
