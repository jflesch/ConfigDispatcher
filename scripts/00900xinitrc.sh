#!/usr/bin/env bash

source ./scripts/common

if [ $XORG -eq 0 ] || [ $AWESOME -eq 0 ]
then exit 0
fi

config_get_value "window_manager" "Window manager" gnome awesome
wm=${value}

config_get_value "start_apps" "Automatically start apps" y n
apps=${value}

echo -n "Generating ${HOME}/.Xclients ... "
rm -f ${HOME}/.xinitrc
echo "$(which xhost) +local:" >> ${HOME}/.xinitrc

if [ ${wm} = "awesome" ]; then
	echo $(which killall) ssh-agent >> ${HOME}/.xinitrc
	echo "ssh-agent > ~/.ssh/agent" >> ${HOME}/.xinitrc
	echo ". ~/.ssh/agent" >> ${HOME}/.xinitrc
	if [ $GNOME_SETTINGS_DAEMON -gt 0 ]
	then echo "$(which gnome-settings-daemon) &" >> ${HOME}/.xinitrc
	# this one may be tricky on FreeBSD and some other systems
	elif [ -x ${USR_LOCAL}/libexec/gnome-settings-daemon ]
	then echo "${USR_LOCAL}/libexec/gnome-settings-daemon &" >> ${HOME}/.xinitrc
	elif [ ${apps} = "y" ] && [ ${RHYTHMBOX} -gt 0 ] && [ ${CMUS} -eq 0 ]
	then echo -e ${ANSI_YELLOW_TXT}"Warning: "${ANSI_RST_TXT}" You need gnome-settings-daemon to use Rhythmbox, but I can't find it"
	fi

	if [ $GNOME_POWER_MANAGER -gt 0 ]
	then echo "$(which gnome-power-manager) &" >> ${HOME}/.xinitrc
	fi
	if [ $UPDATE_NOTIFIER -gt 0 ]
	then echo "$(which update-notifier) &" >> ${HOME}/.xinitrc
	fi
	if [ $PASYSTRAY -gt 0 ]
	then echo "$(which pasystray) &" >> ${HOME}/.xinitrc
	fi
fi

if [ ${apps} = "y" ]; then
	if [ $RHYTHMBOX -gt 0 ]
	then echo "$(which rhythmbox) &" >> ${HOME}/.xinitrc
	fi
	if [ -n "${CHROME}" ]
	then echo "$(which ${CHROME}) &" >> ${HOME}/.xinitrc
	else echo "$(which ${FIREFOX}) &" >> ${HOME}/.xinitrc
	fi
fi

if [ ${wm} = "awesome" ]; then
	if [ $NM_APPLET -gt 0 ]
	then echo "$(which nm-applet) &" >> ${HOME}/.xinitrc
	fi
	if [ $BLUETOOTH_APPLET -gt 0 ]
	then echo "$(which bluetooth-applet) &" >> ${HOME}/.xinitrc
	fi
	if [ $GNOME_VOLUME_CONTROL_APPLET -gt 0 ]
	then echo "$(which gnome-volume-control-applet) &" >> ${HOME}/.xinitrc
	fi
	if [ $SMART_NOTIFIER -gt 0 ]
	then echo "$(which smart-notifier) &" >> ${HOME}/.xinitrc
	fi
	if [ $REDSHIFT -gt 0 ]
	then
		# 50.606:3.154 = Villeneuve d'ascq
		echo "$(which redshift-gtk) -l 50.606:3.154 &" >> ${HOME}/.xinitrc
	fi
fi

# just to be sure
echo "xrdb ${HOME}/.Xresources" >> ${HOME}/.xinitrc

if [ ${wm} = "awesome" ]; then
	## With FreeBSD + Xcompmgr, Xorg eat more and more CPU. At some point,
	## things are just too slow
	#if [ $XCOMPMGR -gt 0 ]
	#then
	#	echo "$(which xcompmgr) -c -f &" >> ${HOME}/.xinitrc
	#fi

	cp -f config/set_session_custom.sh ${HOME}/bin/set_session_custom.sh
	chmod +x ${HOME}/bin/set_session_custom.sh
	echo "${HOME}/bin/set_session_custom.sh &" >> ${HOME}/.xinitrc
fi

if [ ${wm} = "awesome" ]; then
	if [ $CK_LAUNCH_SESSION -gt 0 ]
	then echo "$(which ck-launch-session) $(which dbus-launch) $(which awesome)" >> ${HOME}/.xinitrc
	else echo "$(which awesome)" >> ${HOME}/.xinitrc
	fi
elif [ ${wm} = "gnome" ]; then
	if [ $CK_LAUNCH_SESSION -gt 0 ]
	then echo "$(which ck-launch-session) $(which gnome-session)" >> ${HOME}/.xinitrc
	else echo "$(which gnome-session)" >> ${HOME}/.xinitrc
	fi
fi

chmod +x ${HOME}/.xinitrc
rm -f ${HOME}/.Xclients
ln -s ${HOME}/.xinitrc ${HOME}/.Xclients
rm -f ${HOME}/.xsession
ln -s ${HOME}/.xinitrc ${HOME}/.xsession
chmod +x ${HOME}/.xinitrc
echo -e ${ANSI_GREEN_TXT}"OK"${ANSI_RST_TXT}
