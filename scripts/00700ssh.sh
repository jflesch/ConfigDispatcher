#!/usr/bin/env bash

source ./scripts/common

config_get_value ssh_control_master "SSH control master" "yes" "no" "screw you guys, I'm going home."
ssh_control_master=${value}

mkdir -p ${HOME}/.ssh
chmod 700 ${HOME}/.ssh
rm -rf ${HOME}/.ssh/control-master
rm -f ${HOME}/.ssh/config
touch ${HOME}/.ssh/config

if [ "${value}" = "yes" ]
then
    echo "ControlMaster auto" >> ${HOME}/.ssh/config
    echo "ControlPath /tmp/ssh-%r@%h:%p" >> ${HOME}/.ssh/config
    echo >> ${HOME}/.ssh/config
fi

rm -rf ${HOME}/.ssh/control-master
cat config/ssh_config >> ${HOME}/.ssh/config
