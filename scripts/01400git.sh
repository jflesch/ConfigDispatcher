#!/usr/bin/env bash

source ./scripts/common

if [ ${GIT} -eq 0 ]
then exit 0
fi

config_get_value git_email "Email address to use for Git ?" ext-jflesch@parkeon.com jflesch@openpaper.work jflesch@kwain.net
email=$value

if ! [ -e ${HOME}/.git/config ] # not smashing my work config would actually be nice
then
	cp -f config/gitconfig ${HOME}/.gitconfig
	modify_file_var ${HOME}/.gitconfig "%EMAIL%" $email
fi

git_clone_or_pull "${HOME}/git/git-cascade" "https://github.com/cool-RR/git-cascade.git"
ln -s ${HOME}/git/git-cascade/git-cascade ~/bin
ln -s ${HOME}/git/git-cascade/git-forward-merge ~/bin
