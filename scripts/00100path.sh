#!/usr/bin/env bash

source ./scripts/common

function delete_old_dir()
{
	rmdir $1 2>/dev/null
	rm -f $1
	if [ -e $1 ]
	then
		echo -e ${ANSI_RED_TXT}"Hu?"${ANSI_RST_TXT}" something is already there (rep: $1)."
		exit 1
	fi
}

function make_dir()
{
	dir=$1
	if ! [ -e $dir ]
	then
		echo -e "Creating "${ANSI_YELLOW_TXT}${dir}${ANSI_RST_TXT}
		mkdir -p ${dir}
	fi
}

make_dir ${HOME}/bin
make_dir ${HOME}/download
make_dir ${HOME}/git
make_dir ${HOME}/rsync
make_dir ${HOME}/isos
make_dir ${HOME}/svn
make_dir ${HOME}/tmp

if [ $XORG -gt 0 ] && [ $MUSIC_PLAYER -gt 0 ]
then
    make_dir ${HOME}/Musique
fi
