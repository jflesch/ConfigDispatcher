#!/usr/bin/env bash

source ./scripts/common

function delete_old_dir()
{
	rmdir $1 2>/dev/null
	rm -f $1
	if [ -e $1 ]
	then
		echo -e ${ANSI_RED_TXT}"Hu?"${ANSI_RST_TXT}" something is already there (rep: $1)."
		exit 1
	fi
}

function link_dir()
{
	echo -n "Linking $1 -> $2 ... "
	do_or_fail ln -s "$1" "$2"
}

config_get_value "dirs" "Directories to checkout (y/n/m)" y n m
c=${value}

case "${c}" in 
	"y"|"Y")
		echo "Complete"
		delete_old_dir ${HOME}/Documents
		delete_old_dir ${HOME}/Images/photos
		delete_old_dir ${HOME}/Images/wallpapers
		rm -f ${HOME}/Images ; mkdir ${HOME}/Images
		link_dir ${HOME}/Sync/documents ${HOME}/Documents
		link_dir ${HOME}/Sync/photos ${HOME}/Images
		link_dir ${HOME}/Sync/wallpapers ${HOME}/Images
		;;
	"p"|"P"|"m"|"M")
		echo "Partial"
		delete_old_dir ${HOME}/Images/wallpapers
		rm -f ${HOME}/Images ; mkdir ${HOME}/Images
		link_dir ${HOME}/Sync/wallpapers ${HOME}/Images
		;;
	"n"|"N")
		echo "None"
		;;
esac
