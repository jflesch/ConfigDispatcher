#!/usr/bin/env bash

source ./scripts/common

if [ $(hostname -s) = "alpha" ]
then exit 0
fi

mkdir -p ${HOME}/.ssh

if ! [ -f .ssh/authorized_keys2 ]
then
	echo -n "Downloading basic authorized_keys2 file ... "
	do_or_fail rsync -e "ssh ${SSH_OPTS}" \
		alpha.kwain.net:.ssh/authorized_keys2	\
		${HOME}/.ssh
fi

for key_name in id_rsa fedora id_rsa2
do
	if [ -f ${HOME}/.ssh/${key_name} ]
	then old_key="$(${SHA256} ${HOME}/.ssh/${key_name})"
	else old_key=""
	fi

	echo -n "Updating SSH key ${key_name} ..."
	do_or_fail rsync -e "ssh ${SSH_OPTS}" \
		alpha.kwain.net:.ssh/${key_name}		\
		alpha.kwain.net:.ssh/${key_name}.pub	\
		${HOME}/.ssh

	new_key="$(${SHA256} ${HOME}/.ssh/${key_name})"

	if [ "${old_key}" = "${new_key}" ]
	then
		echo -e "SSH key ${key_name} ${ANSI_GREEN_TXT}unchanged${ANSI_RST_TXT}"
	else
		echo -e "SSH key ${key_name} ${ANSI_YELLOW_TXT}changed${ANSI_RST_TXT}"
	fi
done

