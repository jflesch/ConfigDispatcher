#!/usr/bin/env bash

source ./scripts/common

if [ ${SUBVERSION} -eq 0 ]
then exit 0
fi

echo -n "Update clean_unversionned.py ..."
do_or_fail cp -f config/clean_unversionned.py ~/bin
