#!/usr/bin/env bash

source ./scripts/common

if [ ${OPENVPN} -eq 0 ]
then
	exit 0
fi

config_get_value "openvpn_kwain_net" "VPN access to *.tun.kwain.net" "yes" "no"
do_openvpn_conf=${value}

if [ "${do_openvpn_conf}" != "yes" ]
then exit 0
fi

etc_path=/etc/openvpn
restart_cmd="/etc/init.d/openvpn restart"
if [ -d /usr/local/etc/openvpn ]
then
	etc_path=/usr/local/etc/openvpn
fi

if [ -e /usr/local/etc/rc.d/openvpn ]
then
	restart_cmd="/usr/local/etc/rc.d/openvpn restart"
fi

old_pwd=${PWD}

if [ -d ${HOME}/git/openvpn ]
then
	echo -n "OpenVPN config repository already checked out, let's just update it ... "
	cd ${HOME}/git/openvpn
	if ! git config remote.origin.url > /dev/null
	then
		do_or_fail git checkout -f
	else
		do_or_fail git pull
	fi
else
	cd ${HOME}/git
	echo -n "Checking out openvpn configs ... "
	do_or_fail git clone alpha.kwain.net:git/openvpn
	cd openvpn
fi

cd configs
config_get_value "openvpn_config" "OpenVPN config type" "disable" *
if [ "${value}" = "disable" ]; then
	exit 0
fi
config_file="${value}"
cd ..

cd keys
config_get_value "openvpn_host" "Key set" $(ls -- *.key | ${SED} "s/.key//")
key_set="${value}"
cd ..

echo -n "Generating config file ... "
tmp_config=$(mktemp /tmp/openvpn.conf.XXXXXX)
${SED} "s/%HOST%/${key_set}/g" configs/${config_file} >| ${tmp_config}
echo -e ${ANSI_GREEN_TXT}"OK"${ANSI_RST_TXT}

if ! [ -e ${etc_path}/openvpn.conf ] || ! diff ${etc_path}/openvpn.conf ${tmp_config} > /dev/null
then
	echo "Will need root access to update then OpenVPN config"
	sudo cp -f ${tmp_config} ${etc_path}/openvpn.conf
	sudo chmod a+r ${etc_path}/openvpn.conf
	sudo rm -rf ${etc_path}/keys
	sudo ln -s ${HOME}/git/openvpn/keys ${etc_path}/keys
# we copy the script: because we don't want scripts of the current user to be run as root
# without having something like sudo asking first for a password
	sudo cp -f ${HOME}/git/openvpn/scripts/* ${etc_path}
	sudo ${restart_cmd}
	echo "Config update done"
else
	echo "Nothing to update"
fi

rm -f ${tmp_config}

cd ${old_pwd}

