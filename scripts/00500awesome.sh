#!/usr/bin/env bash

source ./scripts/common

if [ $XORG -eq 0 ] || [ $AWESOME -eq 0 ]
then exit 0
fi

AWESOME_VERSION=42
if awesome --version | head -n 1 | grep "3\.4" > /dev/null
then
    echo -e "Awesome "${ANSI_YELLOW_TXT}"version 3.4"${ANSI_RST_TXT}
    AWESOME_VERSION=34
elif awesome --version | head -n 1 | grep "3\.3" > /dev/null
then
    echo -e "Awesome "${ANSI_YELLOW_TXT}"version 3.3"${ANSI_RST_TXT}" (no cmus support)"
    AWESOME_VERSION=33
fi

AWESOME_RC=config/awesome_${AWESOME_VERSION}_rc.lua
AWESOME_THEME=config/awesome_${AWESOME_VERSION}_theme.lua
AWESOME_MAGNIFIER=config/awesome_${AWESOME_VERSION}_custommagnifier.lua

echo -n "Updating ${HOME}/bin/set_background.sh ... "
do_or_fail cp -f config/set_background.sh ${HOME}/bin/set_background.sh
chmod +x ${HOME}/bin/set_background.sh

echo -n "Updating ${HOME}/bin/set_volume.sh ... "
do_or_fail cp -f config/set_volume.sh ${HOME}/bin/set_volume.sh
chmod +x ${HOME}/bin/set_volume.sh

rm -rf ${HOME}/.config/awesome
mkdir -p ${HOME}/.config/awesome 2> /dev/null

echo "Updating Awesome config ..."
for in_file in config/awesome/awesome_${AWESOME_VERSION}_*
do
	out_file=$(basename $in_file | sed s/awesome_${AWESOME_VERSION}_//)
	echo -n "- ${out_file} ... "
	do_or_fail cp -f ${in_file} ${HOME}/.config/awesome/${out_file}
done



echo "Modifying ${HOME}/.config/awesome/rc.lua ..."

if [ ${CMUS} -gt 0 ] && [ $AWESOME_VERSION = "34" ]
then
    echo -n "- Patching awesome/rc.lua for Cmus ... "
    cd ${HOME}/.config/awesome
    do_or_fail patch -p0 < ${OLDPWD}/patchs/awesome_34_rc.lua-cmus.patch
    cd $OLDPWD
fi
if [ ${SYNERGY} -gt 0 ] && [ $AWESOME_VERSION = "34" ]
then
    echo -n "- Patching awesome/rc.lua for Synergy ... "
    cd ${HOME}/.config/awesome
    do_or_fail patch -p0 < ${OLDPWD}/patchs/awesome_34_rc.lua-synergy.patch
    cd $OLDPWD
fi
echo -e ${ANSI_GREEN_TXT}"OK"${ANSI_RST_TXT}

echo -n "- Replacing variables ... "
ESC_HOME=$(echo ${HOME} | ${SED} s/"\/"/"\\\\\/"/g)
${SED} s/"\${HOME}"/${ESC_HOME}/g ${HOME}/.config/awesome/rc.lua > ${HOME}/.config/awesome/rc.lua.copy
mv -f ${HOME}/.config/awesome/rc.lua.copy ${HOME}/.config/awesome/rc.lua
echo -e ${ANSI_GREEN_TXT}OK${ANSI_RST_TXT}


if [ ${AWESOME_VERSION} -lt 40 ] ; then
    echo -n "Modifying ${HOME}/.config/awesome/theme.lua ... "
    do_or_fail cp -f ${HOME}/.config/awesome/theme.lua ${HOME}/tmp/theme.lua
    if [ ${USR_LOCAL} = "/usr/local" ]
    then
        echo -n "- Fixing paths"
        ${SED} s/\\/usr/\\/usr\\/local/g ${HOME}/tmp/theme.lua > ${HOME}/tmp/theme.lua2
        mv -f ${HOME}/tmp/theme.lua2 ${HOME}/tmp/theme.lua
        echo -e ${ANSI_GREEN_TXT}"OK"${ANSI_RST_TXT}
    fi
    cp -f ${HOME}/tmp/theme.lua ${HOME}/.config/awesome/theme.lua
    rm -f ${HOME}/tmp/theme.lua* 2> /dev/null
    echo -e ${ANSI_GREEN_TXT}"OK"${ANSI_RST_TXT}
fi

if [ -x gconftool ]
then
    # just so we are sure that gnome won't make any troubles
    gconftool-2 --set --type boolean /apps/nautilus/preferences/show_desktop 0
fi
