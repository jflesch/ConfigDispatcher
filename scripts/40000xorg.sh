#!/usr/bin/env bash

source ./scripts/common

if [ $XORG -eq 0 ]
then exit 0
fi

echo -n "- xorg.conf ... "
if ! [ -e /etc/X11/xorg.conf ]
then
    echo
    echo -e "Xorg "${ANSI_YELLOW_TXT}"not configured"${ANSI_RST_TXT}" yet"
	config_get_value "want_xorg_configure" "Want me to run 'Xorg -configure' as root ?" "yes" "no"
    if [ "${value}" = "yes" ]
    then
		if ! sudo Xorg -configure
		then
			echo -e "Xorg autoconfiguration "${ANSI_RED_TXT}"failed"${ANSI_RST_TXT}
			exit 1
		fi
		sudo mv /root/xorg.conf.new /etc/X11/xorg.conf
	else
		echo "OK, doing nothing"
		exit 0
	fi
fi

if grep "\"nv\"" /etc/X11/xorg.conf > /dev/null && grep -i nvidia
then
    echo
    echo -e "Currently using "${ANSI_YELLOW_TXT}"opensource driver"${ANSI_RST_TXT}"."
    echo -n "Install the proprietary driver ? [y/n] "
    read answer
    if [ "$answer" = "y" ]
    then
	do_install "Nvidia drivers" nvidia-glx nvidia x11/nvidia-driver
	if [ -z "${ARCHLINUX}" ] || [ $ARCHLINUX -eq 0 ]
	then do_install "Nvidia-settings" nvidia-settings "" x11/nvidia-settings
	fi

	if [ ${OS} = "FreeBSD" ]
	then
	    echo "- Updating /boot/loader.conf ..."
	    if ! cp -f /boot/loader.conf ${HOME}/tmp/loader.conf
	    then
		echo "can't read loader.conf"
		exit 1
	    fi
	    if ! cp -f /etc/rc.conf ${HOME}/tmp/rc.conf
	    then
		echo "can't read rc.conf"
		exit 1
	    fi
	    sudo echo 'nvidia_load="YES"' >> ${HOME}/tmp/loader.conf
	    sudo echo 'linux_enable="YES"' >> ${HOME}/tmp/rc.conf
	    sudo mv -f ${HOME}/tmp/loader.conf /boot/loader.conf
	    sudo mv -f ${HOME}/tmp/rc.conf /boot/rc.conf
	fi
	echo "- Updating xorg.conf ..."
	${SED} s/\"nv\"/\"nvidia\"/g /etc/X11/xorg.conf > ${HOME}/tmp/xorg.conf
	sudo mv -f ${HOME}/tmp/xorg.conf /etc/X11/xorg.conf
    else
	echo "OK, keeping the opensource one"
    fi
fi

# Just here as a reminder in case I need it.
# Not really checked, because it's more and more managed by Hal now.
KEYBOARD_XORG_CONFIG='
Section "InputDevice"
	Identifier     "Keyboard0"
	Driver         "kbd"
	Option	   "XkbRules"	"xorg"
	Option	   "XkbModel"	"pc105"
	Option	   "XkbLayout"	"(fr|us)"
EndSection
'

echo -e ${ANSI_GREEN_TXT}"OK"${ANSI_RST_TXT}

SYNAPTICS_XORG_CONFIG='
Section "InputDevice"
    Identifier "XXXXX"
    Driver "synaptics"
    Option "Protocol" "auto-dev"
    Option "SendCoreEvents" "true"
    Option "SHMConfig" "true"
EndSection
'

dmesg | grep -i trackpad > /dev/null
if [ $? -gt 0 ]
then has_synaptics=0
else has_synaptics=1
fi

if [ ${has_synaptics} -gt 0 ]
then
    echo "- Trackpad detected, looking for Synaptic configuration"

    echo -n "  - xorg.conf:synaptics ... "
    if ! grep "\"synaptics\"" /etc/X11/xorg.conf > /dev/null
    then
	echo -e ${ANSI_RED_TXT}"KO"${ANSI_RST_TXT}
	echo "Please add the following to /etc/X11/xorg.conf:"
	echo "${SYNAPTICS_XORG_CONFIG}"
	exit 1
    fi
    echo -e ${ANSI_GREEN_TXT}"OK"${ANSI_RST_TXT}

    echo -n "  - xorg.conf:synaptics:SHMConfig ... "
    if ! grep "\"SHMConfig\"" /etc/X11/xorg.conf | grep "true" > /dev/null
    then
	echo -e ${ANSI_RED_TXT}"KO"${ANSI_RST_TXT}
	echo "Please add the following to /etc/X11/xorg.conf:"
	echo "${SYNAPTICS_XORG_CONFIG}"
	exit 1
    fi
    echo -e ${ANSI_GREEN_TXT}"OK"${ANSI_RST_TXT}

    check_cmd Synclient synclient 1 xserver-xorg-input-synaptics xf86-input-synaptics x11-drivers/synaptics
    check_cmd Gsynaptics gsynaptics 1 gsynaptics gsynaptics x11/gsynaptics

    echo -n "  - Updating synaptics script ... "
    cp -f config/synaptics.sh ${HOME}/bin/synaptics.sh
    echo -e ${ANSI_GREEN_TXT}"OK"${ANSI_RST_TXT}

    if ! grep synaptic ${HOME}/.xinitrc > /dev/null
    then
	echo -n "  - Updating .xinitrc for Synaptics ... "
	echo "${HOME}/bin/synaptics.sh wait &" >> ${HOME}/.xinitrc.bis
	cat ${HOME}/.xinitrc >> ${HOME}/.xinitrc.bis
	rm -f ${HOME}/.xinitrc
	mv ${HOME}/.xinitrc.bis ${HOME}/.xinitrc
	echo -e ${ANSI_GREEN_TXT}"OK"${ANSI_RST_TXT}
    fi
fi
