#!/bin/bash

source ./scripts/common

if [ "${DEBIAN}" -le 0 ]
then
	exit 0
fi

if ! lspci | grep -i centrino > /dev/null ;
then
	exit 0
fi

echo "Intel wifi chip detected"

if [ -e /lib/firmware/iwlwifi-7265-9.ucode ] ;
then
	echo "Intel wifi module already installed"
	exit 0
fi

echo -n "Installing Intel wifi drivers ... "
do_or_fail sudo apt-get -y install firmware-iwlwifi
