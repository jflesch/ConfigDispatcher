#!/usr/bin/env bash

source scripts/common

if [ -z "${FIREFOX}" ]
then exit 0
fi

echo -n "Installing vimperator config ... "
do_or_fail cp -f config/vimperatorrc ${HOME}/.vimperatorrc

