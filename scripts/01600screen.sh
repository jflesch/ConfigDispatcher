#!/usr/bin/env bash

source scripts/common

echo -n "Installing screen config ... "
do_or_fail cp -f config/screenrc ${HOME}/.screenrc

