#!/usr/bin/env bash

if [ ${LIRC} -eq 0 ]
then exit 0
fi

echo -n "Installing lircrc ... "

rm -rf ${HOME}/.lircrc
rm -rf ${HOME}/.lirc

cp -f config/lirc/lircrc ${HOME}/.lircrc
cp -Rf config/lirc/lirc ${HOME}/.lirc

echo -e ${ANSI_GREEN_TXT}"OK"${ANSI_RST_TXT}

