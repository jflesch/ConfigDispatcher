#!/usr/bin/env bash

if [ -x "${OS}" ]
then
    echo "Do not run this script directly."
    exit 1
fi

for script in $(ls -1 scripts/*.sh | sort)
do
    if [ "${script}" = "scripts/main.sh" ]
    then continue
    fi
    echo "* "$(basename ${script})
    if ! ${script}
    then
	echo "${script} "${ANSI_RED_TXT}"failed"${ANSI_RST_TXT}
	exit 1
    fi
done
