#!/usr/bin/env bash

source ./scripts/common

echo -n "Deploying chroot.sh ..."
rm -f ${HOME}/bin/chroot.sh
do_or_fail cp -f config/chroot.sh ${HOME}/bin
