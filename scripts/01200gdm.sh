#!/usr/bin/env bash

source scripts/common

if [ "${XORG}" -eq 0 ] || [ "${GDM}" -eq 0 ]
then exit 0
fi

echo -n "- Looking for X11 sessions directory ... "
GDM_SESSION_DIR=/etc/X11/sessions
if ! [ -d ${GDM_SESSION_DIR} ]
then GDM_SESSION_DIR=/usr/share/xsessions
fi
if ! [ -d ${GDM_SESSION_DIR} ]
then GDM_SESSION_DIR=/usr/local/share/xsessions
fi
if ! [ -d ${GDM_SESSION_DIR} ]
then
    echo -e ${ANSI_RED_TXT}"KO"${ANSI_RST_TXT}": Can't find X11 session directory"
    exit 1
fi
echo -e ${ANSI_GREEN_TXT}"OK"${ANSI_RST_TXT}" (${GDM_SESSION_DIR})"

echo -n "- Looking for .xinitrc support to GDM ... "

if [ -e ${GDM_SESSION_DIR}/xinitrc.desktop ]
then
    echo -e ${ANSI_GREEN_TXT}"OK"${ANSI_RST_TXT}
    exit 0
fi
echo -e ${ANSI_YELLOW_TXT}"KO"${ANSI_RST_TXT}

echo -n "- Adding .xinitrc support to GDM ... "

sudo cp -f config/gdm_xinitrc.sh /usr/bin/xinitrc
sudo chmod +x /usr/bin/xinitrc
sudo cp -f config/gdm_xinitrc.desktop ${GDM_SESSION_DIR}/xinitrc.desktop

echo -e ${ANSI_GREEN_TXT}"OK"${ANSI_RST_TXT}
