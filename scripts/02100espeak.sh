#!/usr/bin/env bash

source ./scripts/common

if ! [ -x "$(which espeak 2> /dev/null)" ]
then exit 0
fi

# the following value will be directly read and used by ~/bin/espk.sh
config_get_value "espeak_voice" "Espeak voice ?" "male" "female"

cp -f config/espk.sh ${HOME}/bin/
chmod +x ${HOME}/bin/espk.sh

