#!/usr/bin/env bash

source ./scripts/common

if [ ${LNAV} -eq 0 ]
then exit 0
fi

mkdir -p ~/.lnav/formats/installed

for cfg in android-logcat2.json android-logcat.json pytest_ts.json ;
do
	echo -n "Update lnav config ${cfg} ..."
	do_or_fail cp -f config/lnav/${cfg} ~/.lnav/formats/installed
done
