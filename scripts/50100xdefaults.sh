#!/usr/bin/env bash

source ./scripts/common

if [ -z "$FIREFOX" ] && [ -z "$CHROME" ]
then exit 0
fi

rm -f ${HOME}/bin/firefox-tab.sh
rm -f ${HOME}/.Xdefaults
rm -f ${HOME}/.Xresources

out_sh=${HOME}/bin/firefox-tab.sh
echo "#!/bin/sh" > ${out_sh}
if [ -n "$CHROME" ]
then echo "$(which ${CHROME}) \"\$@\"" >> ${out_sh}
else echo "$(which ${FIREFOX}) -new-tab \"\$@\"" >> ${out_sh}
fi
chmod +x ${out_sh}

ESC_HOME=$(echo ${HOME} | ${SED} s/"\/"/"\\\\\/"/g)
${SED} s/"\${HOME}"/${ESC_HOME}/g config/Xdefaults > ${HOME}/.Xdefaults

ln -s ${HOME}/.Xdefaults ${HOME}/.Xresources

xrdb ${HOME}/.Xdefaults 2>/dev/null >&2
exit 0
