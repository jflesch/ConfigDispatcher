#!/usr/bin/env bash

source ./scripts/common

REPOS="nerdtree=https://github.com/scrooloose/nerdtree.git
syntastic=https://github.com/vim-syntastic/syntastic.git
vim-sleuth=https://github.com/tpope/vim-sleuth.git
vim-vala=https://github.com/arrufat/vala.vim.git"

if [ $VIM -eq 0 ]
then exit 0
fi

rm -f ${HOME}/.vimrc
rm -rf ${HOME}/.vim

cp -f config/vimrc ${HOME}/.vimrc
mkdir -p ${HOME}/.vim/bundle
cp -Ra config/vimrc.d/* ${HOME}/.vim

echo -n "Updating autoload/pathogen ... "
do_or_fail wget https://raw.githubusercontent.com/tpope/vim-pathogen/master/autoload/pathogen.vim \
	-O ${HOME}/.vim/autoload/pathogen.vim

for repo in ${REPOS} ; do
	repodir=$(echo ${repo} | cut -f1 -d=)
	repourl=$(echo ${repo} | cut -f2 -d=)

	git_clone_or_pull "${repodir}" "${repourl}"

	ln -s ${HOME}/git/${repodir} ${HOME}/.vim/bundle
done

if ! [ -e ~/bin/checkstyle-8.2-all.jar ] ; then
	echo -n "Downloading checkstyle 8.2 ..."
	do_or_fail wget -q https://github.com/checkstyle/checkstyle/releases/download/checkstyle-8.2/checkstyle-8.2-all.jar -O ~/bin/checkstyle-8.2-all.jar
fi

cp -f config/sun_checks.xml ~/bin
cp -f config/eclipse-checkstyle-android-best-practices.xml ~/bin
cp -f config/splintrc ~/.splintrc

mkdir -p ~/.config
cp -f config/pep8rc ~/.config/pep8
if ! [ -e ~/.config/pycheckstyle ] ; then
	ln -s ~/.config/pep8 ~/.config/pycheckstyle
fi

# Nvim support
rm -rf ~/.config/nvim
mkdir -p ~/.config/nvim
echo "Neovim compat ... "
do_or_fail cp config/nvim_init.vim ~/.config/nvim/init.vim
